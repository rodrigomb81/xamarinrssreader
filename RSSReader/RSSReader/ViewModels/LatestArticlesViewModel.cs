﻿using System.Collections.ObjectModel;
using System.Collections.Specialized;
using RSSReader.Models;
using RSSReader.Services;

namespace RSSReader.ViewModels
{
    class LatestArticlesViewModel : BaseViewModel
    {
        public ActivityStatus Status { get; }

        public ObservableCollection<Article> Articles { get; }

        private readonly ArticlesService _articlesService = new ArticlesService();

        public LatestArticlesViewModel()
        {
            Status = new ActivityStatus();
            Articles = _articlesService.Articles;
        }

        public void LoadArticles()
        {
            Status.Start();
            _articlesService.Load();
            Status.Finish();
        }
    }
}
