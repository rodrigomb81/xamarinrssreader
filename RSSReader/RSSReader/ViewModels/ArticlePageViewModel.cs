﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace RSSReader.ViewModels
{
    class ArticlePageViewModel : BaseViewModel
    {
        public string ArticleUrl { 
            get => _articleUrl;
            set
            {
                if (_articleUrl == value) return;
                _articleUrl = value;
                OnPropertyChanged(nameof(ArticleUrl));
            }
        }
        private string _articleUrl;

        public bool IsLoading
        {
            get => _isLoading;
            set
            {
                if (_isLoading == value) return;
                _isLoading = value;
                OnPropertyChanged(nameof(IsLoading));
            }
        }
        private bool _isLoading = true;

        public bool HasLoaded
        {
            get => _hasLoaded;
            set
            {
                if (_hasLoaded == value) return;
                _hasLoaded = value;
                OnPropertyChanged(nameof(HasLoaded));
            }
        }
        private bool _hasLoaded = false;

        public string LoadingMessage
        {
            get => _loadingMessage;
            set
            {
                if (_loadingMessage == value) return;
                _loadingMessage = value;
                OnPropertyChanged(nameof(LoadingMessage));
            }
        }
        private string _loadingMessage;

        private string[] MessagePool = new string[]
        {
            "Reticulando Splines", "Maximizando visitas", "Clickbaiteando articulos",
            "Normalizando comentarios", "Creando SPA React", "Cargando finanzas",
            "Inventando primicias", "Manejando tendencias", "Tweeteando noticias",
            "Escribiendo articulo"
        };

        public ICommand ShowArticle { get; }
        public ICommand ShowLoadingIndicator { get; }

        public ICommand ShowNextLoadingMessage { get; }

        public ArticlePageViewModel()
        {
            ShowArticle = new Command(ShowArticleCommand);
            ShowLoadingIndicator = new Command(ShowLoadingIndicatorCommand);
            ShowNextLoadingMessage = new Command(PickRandomLoadingMessage);
            PickRandomLoadingMessage();
        }

        void ShowArticleCommand()
        {
            IsLoading = false;
            HasLoaded = true;
        }

        void ShowLoadingIndicatorCommand()
        {
            IsLoading = true;
            HasLoaded = false;
        }

        void PickRandomLoadingMessage()
        {
            var rnd = new Random();
            var nextMessage = MessagePool[rnd.Next(MessagePool.Length)];
            LoadingMessage = nextMessage;
        }
    }
}
