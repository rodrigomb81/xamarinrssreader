﻿using RSSReader.Data;
using RSSReader.Models;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Xamarin.Forms;

namespace RSSReader.ViewModels
{
    class FavoriteArticlesViewModel : BaseViewModel
    {
        public ActivityStatus Status { get; }
        public ObservableCollection<Article> Favorites { get => _favorites; }
        private ObservableCollection<Article> _favorites = new ObservableCollection<Article>();

        private FavoriteArticlesRepository favoriteArticlesRepository = new FavoriteArticlesRepository();

        public FavoriteArticlesViewModel()
        {
            Status = new ActivityStatus(); 
            RefreshArticles = new Command(LoadArticles);
        }

        public void LoadArticles()
        {
            Status.Start();
            _favorites.Clear();
            var articles = favoriteArticlesRepository.GetArticles();
            foreach (var article in articles) Favorites.Add(article);
            Status.Finish();
        }

        public void RemoveArticle(Article toBeRemoved)
        {
            _favorites.Remove(toBeRemoved);
            favoriteArticlesRepository.RemoveArticle(toBeRemoved);
        }

        public ICommand RefreshArticles { get; }
    }
}
