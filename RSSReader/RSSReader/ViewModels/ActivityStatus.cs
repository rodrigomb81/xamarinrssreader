﻿namespace RSSReader.ViewModels
{
    class ActivityStatus : BaseViewModel
    {
        public bool HasFinished
        {
            get => !IsWorking;
        }

        public bool IsWorking
        {
            get => _isWorking;
        }
        private bool _isWorking = true;

        public void Finish()
        {
            _isWorking = false;
            OnPropertyChanged(nameof(IsWorking));
            OnPropertyChanged(nameof(HasFinished));
        }

        public void Start()
        {
            _isWorking = true;
            OnPropertyChanged(nameof(IsWorking));
            OnPropertyChanged(nameof(HasFinished));
        }
    }
}
