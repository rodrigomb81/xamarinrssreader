﻿using RSSReader.Models;
using RSSReader.Views.Events;
using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RSSReader.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ArticleViewCell : ViewCell
    {
        public event EventHandler<FavoriteStatusToggledEventArgs> FavoriteStatusToggled;

        public static readonly BindableProperty ArticleSourceProperty =
            BindableProperty.Create("ArticleSource", typeof(Article), typeof(ArticleViewCell));

        public Article ArticleSource
        {
            get => (Article)GetValue(ArticleSourceProperty);
            set => SetValue(ArticleSourceProperty, value);
        }

        public ArticleViewCell()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            ArticleSource.PropertyChanged += ArticleSource_PropertyChanged;
            setToggleFavoriteStyleClass(ArticleSource.IsFavorite);
        }

        protected virtual void OnFavoriteStatusToggled(FavoriteStatusToggledEventArgs e)
        {
            EventHandler<FavoriteStatusToggledEventArgs> handler = FavoriteStatusToggled;
            handler?.Invoke(this, e);
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            ArticleSource.IsFavorite = !ArticleSource.IsFavorite;
            OnFavoriteStatusToggled(new FavoriteStatusToggledEventArgs(ArticleSource));
        }

        private void ArticleSource_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsFavorite")
                setToggleFavoriteStyleClass(ArticleSource.IsFavorite);
        }

        private void setToggleFavoriteStyleClass(bool isFavorite)
        {
            if (isFavorite)
            {
                var styles = new List<string>();
                styles.Add("BotonActivado");
                ToggleFavorite.StyleClass = styles;
            }
            else
            {
                var styles = new List<string>();
                styles.Add("BotonNormal");
                ToggleFavorite.StyleClass = styles;
            }
        }
    }
}