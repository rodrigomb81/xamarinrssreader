﻿using RSSReader.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RSSReader.Views.Events
{
    public class FavoriteStatusToggledEventArgs : EventArgs
    {
        public Article Article { get; set; }

        public FavoriteStatusToggledEventArgs(Article article)
        {
            Article = article;
        }
    }
}
