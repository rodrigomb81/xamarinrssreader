﻿using RSSReader.Data;
using RSSReader.Models;
using RSSReader.ViewModels;
using RSSReader.Views.Events;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace RSSReader.Views
{
    public partial class LatestArticlesPage : ContentPage
    {
        private FavoriteArticlesRepository favoriteArticles = new FavoriteArticlesRepository();
        private LatestArticlesViewModel ViewModel;
        public LatestArticlesPage()
        {
            InitializeComponent();
            ViewModel = (LatestArticlesViewModel) BindingContext;
        }

        protected async override void OnAppearing()
        {
            await Task.Run(LoadArticlesOrShowPopup);
        }

        private async void LoadArticlesOrShowPopup()
        {
            try
            {
                if (ViewModel.Status.HasFinished == false) ViewModel.LoadArticles();
            } catch (Exception)
            {
                await DisplayAlert("Alerta", "Hubo un problema cargando los articulos 🤔", "Ok");
            }
        }

        private void ListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var tappedArticle = (Article)e.Item;
            Navigation.PushAsync(new ArticlePage(tappedArticle.ArticleUrl));
        }

        private async void ArticleViewCell_FavoriteStatusToggled(object sender, FavoriteStatusToggledEventArgs e)
        {
            if (e.Article.IsFavorite)
                await Task.Run(() => favoriteArticles.AddArticle(e.Article));
            else
                await Task.Run(() => favoriteArticles.RemoveArticle(e.Article));
        }
    }
}
