﻿using RSSReader.Models;
using RSSReader.ViewModels;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RSSReader.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FavoriteArticlesPage : ContentPage
    {
        private FavoriteArticlesViewModel ViewModel;
        public FavoriteArticlesPage()
        {
            InitializeComponent();
            ViewModel = (FavoriteArticlesViewModel)BindingContext;
        }

        protected override async void OnAppearing()
        {
            await Task.Run(() => ViewModel.LoadArticles());
        }

        private async void ArticleViewCell_FavoriteStatusToggled(object sender, Events.FavoriteStatusToggledEventArgs e)
        {
            e.Article.IsFavorite = false;
            await Task.Run(() => ViewModel.RemoveArticle(e.Article));
        }
    }
}