﻿using RSSReader.ViewModels;
using System;
using System.Diagnostics;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RSSReader.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ArticlePage : ContentPage
    {
        private readonly ArticlePageViewModel ViewModel;

        public ArticlePage(string articleUrl)
        {
            InitializeComponent();
            ViewModel = (ArticlePageViewModel)BindingContext;
            ViewModel.ArticleUrl = articleUrl;
            Device.StartTimer(TimeSpan.FromSeconds(2), OnTimerTick);
        }

        private void WebView_Navigated(object sender, WebNavigatedEventArgs e)
        {
            if (ViewModel.ShowArticle.CanExecute(null)) ViewModel.ShowArticle.Execute(null);
        }

        private bool OnTimerTick()
        {
            if (ViewModel.HasLoaded) return false;
            if (ViewModel.ShowNextLoadingMessage.CanExecute(null)) ViewModel.ShowNextLoadingMessage.Execute(null);
            return true;
        }
    }
}