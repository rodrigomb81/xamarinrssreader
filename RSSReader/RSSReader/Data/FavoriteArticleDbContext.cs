﻿using Microsoft.EntityFrameworkCore;
using RSSReader.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Xamarin.Essentials;

namespace RSSReader.Data
{
    class FavoriteArticleDbContext : DbContext
    {
        public DbSet<Article> Articles { get; set; }

        public FavoriteArticleDbContext()
        {
            SQLitePCL.Batteries_V2.Init();

            this.Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            string dbPath = Path.Combine(FileSystem.AppDataDirectory, "articles3.db");
            options.UseSqlite($"Filename={dbPath}");
        }
    }
}
