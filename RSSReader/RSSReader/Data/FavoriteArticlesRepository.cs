﻿using RSSReader.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace RSSReader.Data
{
    class FavoriteArticlesRepository
    {
        public List<Article> GetArticles()
        {
            using var db = new FavoriteArticleDbContext();
            return db.Articles.ToList();
        }

        public void AddArticle(Article article)
        {
            using var db = new FavoriteArticleDbContext();
            db.Add(article);
            db.SaveChanges();
        }

        public void RemoveArticle(Article toBeRemoved)
        {
            using var db = new FavoriteArticleDbContext();
            var storedArticle = db.Articles
                .Where(article => article.Title == toBeRemoved.Title)
                .FirstOrDefault();
            if (storedArticle != null)
            {
                db.Remove(storedArticle);
                db.SaveChanges();
            }
        }
    }
}
