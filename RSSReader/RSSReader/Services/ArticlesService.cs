﻿using System.Collections.ObjectModel;
using CodeHollow.FeedReader;
using CodeHollow.FeedReader.Feeds;
using System.Linq;
using RSSReader.Models;

namespace RSSReader.Services
{
    class ArticlesService
    {
        private static string FEED_URL = "https://es.investing.com/rss/news_25.rss";

        public ObservableCollection<Article> Articles = new ObservableCollection<Article>();

        public async void Load()
        {
            var feed = await FeedReader.ReadAsync(FEED_URL);

            var newsItems = feed.Items.Select(mapToArticle);

            foreach (var item in newsItems) Articles.Add(item);
        }

        private Article mapToArticle(FeedItem feedItem)
        {
            var enclosure = ((Rss20FeedItem)feedItem.SpecificItem).Enclosure;

            return new Article() {
                ArticleUrl = feedItem.Link,
                Author = feedItem.Author,
                PublishingDate = feedItem.PublishingDate,
                ThumbnailUrl = enclosure.Url,
                Title = feedItem.Title
            };
        }
    }
}
