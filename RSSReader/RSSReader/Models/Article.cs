﻿using System;
using System.ComponentModel;

namespace RSSReader.Models

{
    public class Article : INotifyPropertyChanged
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string ThumbnailUrl { get; set; }
        public string Author { get; set; }
        public DateTime? PublishingDate { get; set; }
        public string ArticleUrl { get; set; }
        public bool IsFavorite { 
            get => _isFavorite;
            set
            {
                if (_isFavorite != value)
                {
                    _isFavorite = value;
                    OnPropertyChanged(nameof(IsFavorite));
                }
            }
        }
        private bool _isFavorite;

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
    }
}
